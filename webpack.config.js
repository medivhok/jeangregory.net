// build dev test
const path = require('path');
const merge = require('webpack-merge');
const webpack = require('webpack');

// Webpack Plugins
const HtmlWebpackPlugin = require('html-webpack-plugin');
// const NpmInstallPlugin = require('npm-install-webpack-plugin');
const WebpackFailPlugin = require('webpack-fail-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

const TARGET = process.env.npm_lifecycle_event;

console.log('Webpack npm TARGET : ' + TARGET);

const PATHS = {
    app: path.join(__dirname, 'src/app'),
    style: path.join(__dirname, 'src/scss'),
    build: path.join(__dirname, 'dist/assets/scripts'),
    report: path.join(__dirname, 'report')
};

const common = {
    target: 'web',
    entry: {
        polyfills: './src/app/polyfills.ts',
        vendor: './src/app/vendor.ts',
        app: './src/app/index.ts'
    },
    output: {
        filename: '[name].js',
        sourceMapFilename: '[name].js.map',
        path: PATHS.build,
        publicPath: '/assets/scripts/'
    },
    resolve: {
        extensions: ['', '.ts', '.js']
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: 'JeanGregory.Net',
            filename: process.cwd() + '/dist/index.html',
            template: 'src/index.html',
            chunksSortMode: packageSort(['polyfills', 'vendor', 'app'])
        }),
        WebpackFailPlugin
    ],
    module: {
        loaders: [{
            test: /\.ts(x?)$/,
            loader: 'ts-loader',
            include: PATHS.app
        }, {
            test: /\.html$/,
            loader: 'html'
        }, {
            test: /\.scss$/,
            loaders: ['style', 'css', 'sass'],
            include: PATHS.style
        }]
    },
    sassLoader: {
        includePaths: [
            'node_modules/foundation-sites/scss',
            'node_modules/motion-ui/src'
        ]
    }
};

const test = {
    debug: true,
    devTool: 'inline-source-map',
    plugins: [
        new CleanWebpackPlugin([PATHS.build, PATHS.report], {
            root: process.cwd(),
            verbose: true
        })
    ],
    module: {
        postLoaders: [{
            test: /\.(js|ts)$/,
            include: PATHS.app,
            loader: 'istanbul-instrumenter-loader',
            exclude: [/\.spec\.ts$/, /\.e2e\.ts$/, /node_modules/]
        }]
    }
};


if (TARGET === 'start' || !TARGET) {
    module.exports = merge(common, {
        debug: true,
        devServer: {
            historyApiFallback: true,
            hot: true,
            inline: true,
            progress: true,
            stats: 'errors-only',
            host: process.env.HOST,
            port: process.env.PORT
        },
        plugins: [
            new webpack.HotModuleReplacementPlugin()
        ]
    });
}


if (TARGET === 'test') {
    module.exports = merge(common, test);
}

if (TARGET === 'build') {
    module.exports = merge(common, {
        output: {
            path: PATHS.build,
            filename: '[name].[chunkhash].js',
            chunkFilename: '[chunkhash].js',
            sourceMapFilename: '[name].map'
        },
        plugins: [
            new CleanWebpackPlugin([PATHS.build], {
                root: process.cwd(),
                verbose: true
            }),
            new webpack.optimize.CommonsChunkPlugin({
                names: ['vendor', 'manifest']
            }),
            new webpack.optimize.UglifyJsPlugin({
                compress: {
                    warnings: false
                }
            })
        ]
    });
}

// Helper functions
function packageSort(packages) {
    // packages = ['polyfills', 'vendor', 'app']
    var len = packages.length - 1;
    var first = packages[0];
    var last = packages[len];
    return function sort(a, b) {
        // polyfills always first
        if (a.names[0] === first) {
            return -1;
        }
        // main always last
        if (a.names[0] === last) {
            return 1;
        }
        // vendor before app
        if (a.names[0] !== first && b.names[0] === last) {
            return -1;
        } else {
            return 1;
        }
    }
}
