/// <reference path="../../typings/browser.d.ts" />
// /// <reference path="./require.d.ts" />

require('../scss/app.scss');

import { bootstrap } from '@angular/platform-browser-dynamic';
import { ROUTER_PROVIDERS } from '@angular/router';
import { AppComponent } from './app.component';

bootstrap(AppComponent, [ROUTER_PROVIDERS]);
