// Angular 2 imports
import {Type} from '@angular/core';
// JeanGregory.Net imports
import {WidgetModel} from './widget.model';
import {CSSClassInterface} from '../cssclass';

export interface WidgetModelInterface extends CSSClassInterface {
    getType(): Type;

    addWidgetModels(models: Array<WidgetModel>): Array<WidgetModel>;

    removeWidgetModels(models: Array<WidgetModel>): Array<WidgetModel>;

    getWidgetModels(): Array<WidgetModel>;
}
