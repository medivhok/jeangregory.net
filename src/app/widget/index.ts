export {WidgetComponent} from './widget.component';
export {WidgetModel} from './widget.model';
export {WidgetService} from './widget.service';
export {WidgetModelInterface} from './widget-model.interface';
