// Angular2 imports
import {
    Injectable,
    Type,
} from '@angular/core';

// JeanGregory.Net imports
import {WidgetComponent} from './widget.component';
import {WidgetModelInterface} from './widget-model.interface';
import {ItemArray} from '../generics';
import {CSSClassModel} from '../cssclass';

@Injectable()
export class WidgetModel implements WidgetModelInterface {

    protected widgetModels = new ItemArray<WidgetModel>();

    constructor(
        protected cssClass: CSSClassModel,
        protected type: Type = WidgetComponent) {
    }


    // =========================================================================
    // WidgetModelInterface implementation

    getType(): Type {
        return this.type;
    }

    addWidgetModels(models: Array<WidgetModel>): Array<WidgetModel> {
        return this.widgetModels.addItems(models);
    }

    removeWidgetModels(models: Array<WidgetModel>): Array<WidgetModel> {
        return this.widgetModels.removeItems(models);
    }

    getWidgetModels(): Array<WidgetModel> {
        return this.widgetModels.getItems();
    }


    // =========================================================================
    // CSSClassInterface implementation

    addClasses(classesName: string[]): Array<string> {
        return this.cssClass.addClasses(classesName);
    }

    removeClasses(classesName: string[]): Array<string> {
        return this.cssClass.removeClasses(classesName);
    }

    getClassesArray(): Array<string> {
        return this.cssClass.getClassesArray();
    }
}
