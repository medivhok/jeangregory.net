// Angular 2 imports
import {
    Component,
    ComponentResolver,
    HostBinding,
    Input,
    ViewContainerRef
} from '@angular/core';

// JeanGregory.Net imports
import {CSSClassModel} from '../cssclass';
import {WidgetModel} from './widget.model';
// import {WidgetService} from './widget.service';

@Component({
    selector: 'jg-widget',
    template: <string>require('./widget.component.html'),
    directives: [WidgetComponent],
    providers: [ComponentResolver]
})
export class WidgetComponent {

    constructor(
        protected viewContainer: ViewContainerRef,
        protected resolver: ComponentResolver,
        protected cssClasses: CSSClassModel,
        protected widgets: WidgetModel) {
    }

    @HostBinding('class')
    get classesString(): string {
        return this.cssClasses.getClassesArray().join('');
    }

    @Input()
    addClasses(classesName: string[]): Array<string> {
        return this.cssClasses.addClasses(classesName);
    }

    removeClasses(classesName: string[]): Array<string> {
        return this.cssClasses.removeClasses(classesName);
    }
}
