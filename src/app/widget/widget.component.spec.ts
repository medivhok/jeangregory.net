// Angular imports
import {
    describe,
    it,
    expect,
    beforeEach,
    beforeEachProviders,
    inject
} from '@angular/core/testing';
import {TestComponentBuilder, ComponentFixture} from '@angular/compiler/testing';
import {provide} from '@angular/core';

// JeanGregory.Net imports
import {MockCSSClassModel} from '../tests/mocks';
import {CSSClassModel} from '../cssclass';
import {WidgetComponent} from './widget.component';


describe('WidgetComponent', () => {

    beforeEachProviders(() => {
        provide(CSSClassModel, {useClass: MockCSSClassModel});
    });

    // it('communicate well with the CSSClassModel', inject(
    //     [TestComponentBuilder], (tcb: TestComponentBuilder) => {
    //         tcb.overrideProviders(
    //             WidgetComponent,
    //             [provide(CSSClassModel, { useClass: MockCSSClassModel })]);
    //         return tcb.createAsync(WidgetComponent).then((componentFixture: ComponentFixture<WidgetComponent>) => {
    //             let widgetComponent = componentFixture.componentInstance;
    //             componentFixture.detectChanges();
    //
    //             expect(widgetComponent.classesString).toEqual('testClass2');
                // expect(widgetComponent.addClasses(['test1', 'test2']))
                //     .toBe(['test1', 'test2']);
                // expect(this.widgetComponentFixture.removeClasses(['test1', 'test2']))
                //     .toBe(['test1', 'test2']);
                // expect(this.widgetComponentFixture.contains('testClass'))
                //     .toBe(true);
                // expect(this.widgetComponentFixture.getArray())
                //     .toBe(['testClass']);
    //         });
    //     })
    // );
});
