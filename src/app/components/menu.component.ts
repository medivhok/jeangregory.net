// Angular 2 imports
import {Component, HostBinding} from '@angular/core';

// JeanGregory.Net imports
import {CSSClassService, CSSClassModel} from '../cssclass';

@Component({
    selector: 'jg-menu',
    template: 'Menu'
})
export class MenuComponent {
    protected cssClass: CSSClassModel;

    constructor(cssClassService: CSSClassService) {
        this.cssClass = cssClassService.getCSSClassModel();
        this.cssClass.addClass('row');
    }

    @HostBinding('class')
    get classesString(): string {
        return this.cssClass.getClassesArray().join(' ');
    }
}
