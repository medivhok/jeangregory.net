// Angular 2 imports
import {Component, HostBinding} from '@angular/core';

// JeanGregory.Net imports
import {CSSClassService, CSSClassModel} from '../cssclass';

@Component({
    selector: 'jg-page-content',
    template: 'PageContent'
})
export class PageContentComponent {
    protected cssClass: CSSClassModel;

    constructor(cssClassService: CSSClassService) {
        this.cssClass = cssClassService.getCSSClassModel();
        this.cssClass.addClass('row');
    }

    @HostBinding('class')
    get classesString(): string {
        return this.cssClass.getClassesArray().join(' ');
    }
}
