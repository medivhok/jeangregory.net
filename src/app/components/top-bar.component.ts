// Angular 2 imports
import {Component, HostBinding} from '@angular/core';

// JeanGregory.Net imports
import {CSSClassService, CSSClassModel} from '../cssclass';

@Component({
    selector: 'jg-top-bar',
    template: 'TopBar'
})
export class TopBarComponent {
    protected cssClass: CSSClassModel;

    constructor(cssClassService: CSSClassService) {
        this.cssClass = cssClassService.getCSSClassModel();
        this.cssClass.addClasses(['top-bar', 'row']);
    }

    @HostBinding('class')
    get classesString(): string {
        return this.cssClass.getClassesArray().join(' ');
    }
}
