// import {WidgetModel} from '../widget/index';
import {HandlerInterface} from './handler.interface';

export class PipelineManager {
    private handlers: HandlerInterface[] = [];

    addHandler(handler: HandlerInterface) {
        this.handlers.push(handler);
    }

    // doPipeline(widgetModel: WidgetModel) {
    //     this.handlers.forEach(
    //         function(handler) {
    //             handler.process(widgetModel);
    //         });
    // }
}
