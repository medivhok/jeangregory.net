// Angular 2 imports
import {Component} from '@angular/core';
import {Routes, Router, ROUTER_DIRECTIVES} from '@angular/router';

// JeanGregory.Net imports
import {HemisphereChooserComponent, RightHemisphereComponent, LeftHemisphereComponent} from './hemisphere';
import {CSSClassService} from './cssclass';

@Component({
    selector: 'jg-app',
    template: `
        <router-outlet></router-outlet>
    `,
    directives: [ROUTER_DIRECTIVES],
    providers: [CSSClassService]
})

@Routes([
    {path: '/',       component: HemisphereChooserComponent},
    {path: '/droit',  component: RightHemisphereComponent},
    {path: '/gauche', component: LeftHemisphereComponent}
])

export class AppComponent {
    constructor(private _router: Router) {}
}
