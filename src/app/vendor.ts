// https://github.com/AngularClass/angular2-webpack-starter/blob/master/src/vendor.ts

// Angular 2
import '@angular/platform-browser-dynamic';
// import '@angular/platform/common_dom';
import '@angular/core';
import '@angular/router';

// RxJS
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
