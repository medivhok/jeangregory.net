// Angular 2 imports
import {Component} from '@angular/core';

// JeanGregory.Net imports
import {
    TopBarComponent,
    HeaderComponent,
    MenuComponent,
    PageContentComponent,
    FooterComponent
} from '../../components';

/**
 * @RightHemisphereComponent -
 */
@Component({
    selector: 'jg-right-hemisphere',
    template: <string>require('../hemisphere.component.html'),
    directives: [
        TopBarComponent,
        HeaderComponent,
        MenuComponent,
        PageContentComponent,
        FooterComponent
    ]
})
export class RightHemisphereComponent {

}
