// Angular 2 imports
import {Component} from '@angular/core';
import {ROUTER_DIRECTIVES} from '@angular/router';

// JeanGregory.Net

@Component({
    selector: 'hemisphere-chooser',
    template: <string>require('./hemisphere-chooser.component.html'),
    directives: [ROUTER_DIRECTIVES]
})

export class HemisphereChooserComponent {

}
