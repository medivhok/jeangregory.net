// JeanGregory.Net imports
import {CSSClassInterface} from '../../cssclass';

export class MockCSSClassModel implements CSSClassInterface {

    classesString: string = 'testClass';

    addClasses(classesName: string[]): Array<string> {
        return classesName;
    }

    removeClasses(classesName: string[]): Array<string> {
        return classesName;
    }

    contains(className: string): boolean {
        return true;
    }

    getClassesArray(): Array<string> {
        return ['testClass'];
    }
}
