// /// <reference path="./require.d.ts" />

import {setBaseTestProviders} from '@angular/core/testing';

import {
    TEST_BROWSER_STATIC_PLATFORM_PROVIDERS,
    TEST_BROWSER_STATIC_APPLICATION_PROVIDERS
} from '@angular/platform-browser/testing';

setBaseTestProviders(
    TEST_BROWSER_STATIC_PLATFORM_PROVIDERS,
    TEST_BROWSER_STATIC_APPLICATION_PROVIDERS);

let testsContext = require.context('.', true, /\.spec\.ts$/);
testsContext.keys().forEach(testsContext);
