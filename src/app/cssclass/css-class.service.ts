// Angular 2 imports
import {Injectable} from '@angular/core';

// JeanGregory.Net imports
import {CSSClassModel} from './css-class.model';

@Injectable()
export class CSSClassService {
    getCSSClassModel(): CSSClassModel {
        return new CSSClassModel();
    }
}
