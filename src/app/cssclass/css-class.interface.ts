export interface CSSClassInterface {

    addClasses(classesName: string[]): Array<string>;

    removeClasses(classesName: string[]): Array<string>;

    getClassesArray(): Array<string>;
}
