import {describe, it, expect} from '@angular/core/testing';

import {CSSClassModel} from './css-class.model';

describe('CSSClassModel', () => {

    let cssClassModel: CSSClassModel = new CSSClassModel();

    it('returns true if class is added', () => {
        expect(cssClassModel.addClass('testClass')).toBeTruthy;
    });

    it('returns false if class is already in model', () => {
        expect(cssClassModel.addClass('testClass')).toBeFalsy;
    });

    it('returns all the class separated by a space', () => {
        cssClassModel.addClass('testClass2');
        expect(cssClassModel.getClassesArray()).toEqual(['testClass', 'testClass2']);
    });

    it('returns true if class is removed', () => {
        expect(cssClassModel.removeClass('testClass')).toBeTruthy;
        expect(cssClassModel.getClassesArray()).toEqual(['testClass2']);
    });

    it('adds an array of css classes', () => {
        cssClassModel.addClasses(['testClass3', 'testClass4', 'testClass2']);
        expect(cssClassModel.getClassesArray()).toEqual(['testClass2', 'testClass3', 'testClass4']);
    });

    it('removes an array of css classes without errors', () => {
        expect(cssClassModel.removeClasses(['testClass2', 'testClass4', 'testClass5'])).not.toThrowError;
        expect(cssClassModel.getClassesArray()).toEqual(['testClass3']);
    });

    it('returns true if class is in model and false otherwise', () => {
        expect(cssClassModel.contains('testClass3')).toBeTruthy;
        expect(cssClassModel.contains('testClass')).toBeFalsy;
    });

    it('returns a shallow copy of the class string array', () => {
        let testArray: string[] = cssClassModel.getClassesArray();
        testArray.push('testClassArray');
        expect(cssClassModel.contains('testClassArray')).toBeFalsy;
    });
});
