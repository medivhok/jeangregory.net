import {ItemArray} from '../generics';
import {CSSClassInterface} from './css-class.interface';

/**
 * Helper class to help with the use of css classes of a component.
 */
export class CSSClassModel implements CSSClassInterface {
    /**
     * Array that contains the different css classes.
     * @type {string[]}
     */
    protected classes = new ItemArray<string>();

    /**
     * Add a css class the the array.
     * @param  {string}  className The css class to add.
     * @return {boolean}           true if the class was added, false if the class was already present in the array.
     */
    addClass(className: string): boolean {
        return this.classes.addItem(className);
    }

    /**
     * Add an array of css classes.
     * @param  {string[]} newClassArray The array of css classes.
     */
    addClasses(classesName: string[]): Array<string> {
        return this.classes.addItems(classesName);
    }

    /**
     * Remove a css class from the array.
     * @param  {string}  className The name of the css class to be removed.
     * @return {boolean}           true if the css class was removed, false if it was not in the array.
     */
    removeClass(className: string): boolean {
        return this.classes.removeItem(className);
    }

    /**
     * Remove an array of css classes.
     * @param  {string[]} oldClassesArray The array of css classes to be removed.
     */
    removeClasses(classesName: string[]): Array<string> {
        return this.classes.removeItems(classesName);
    }

    /**
     * Verify if the css array contains a specific css class.
     * @param  {string}  className The name of the css class to look for.
     * @return {boolean}           true if the array contains the css class, false if not.
     */
    contains(className: string): boolean {
        return this.classes.contains(className);
    }

    /**
     * Returns an array of the classes curently in this CSSClassModel.
     * @return {Array<string>} Array of class strings.
     */
    getClassesArray(): Array<string> {
        return this.classes.getItems();
    }
}
