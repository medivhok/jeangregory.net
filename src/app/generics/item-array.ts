export class ItemArray<T> {
    protected itemArray: Array<T> = [];

    /**
     * Verify if the array contains a specific item.
     * @param  {T}       item The item to look for.
     * @return {boolean}      true if item is found, false otherwise.
     */
    contains(item: T): boolean {
        let result = false;
        if (0 <= this.itemArray.indexOf(item)) {
            result = true;
        }
        return result;
    }

    /**
     * Add an item to the array.
     * @param  {T}       item Item that will be added to the array.
     * @return {boolean}      true if item is added, false is item already present in the array.
     */
    addItem(item: T): boolean {
        let result = false;
        if (!this.contains(item)) {
            this.itemArray.push(item);
            result = true;
        }
        return result;
    }

    /**
     * Add an array of items in the array.
     * @param  {T[]}      items The array of items to add.
     * @return {Array<T>}       An array of the items added.
     */
    addItems(items: T[]): Array<T> {
        return items.filter(this.addItem, this);
    }

    /**
     * Remove an item from the array.
     * @param  {T}       item Item to be removed.
     * @return {boolean}      true if item was removed, false is it wasn't in the array.
     */
    removeItem(item: T): boolean {
        let result = false;
        let index: number = this.itemArray.indexOf(item);
        if (index >= 0) {
            this.itemArray.splice(index, 1);
            result = true;
        }
        return result;
    }

    /**
     * Remove an array of items.
     * @param  {T[]}      items The array of items to remove.
     * @return {Array<T>}       An array of items that have been removed.
     */
    removeItems(items: T[]): Array<T> {
        return items.filter(this.removeItem, this);
    }

    /**
     * Returns an array of the items curently in this array.
     * @return {Array<T>} A copy of the items array.
     */
    getItems(): Array<T> {
        return this.itemArray.slice();
    }
}
