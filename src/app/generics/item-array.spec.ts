import {describe, it, expect} from '@angular/core/testing';

import {ItemArray} from './item-array';

describe('ItemArray<string>', () => {

    let itemArray = new ItemArray<string>();

    it('returns true if an item is added', function() {
        expect(itemArray.addItem('test')).toBeTruthy();
    });

    it('returns false if item is already in model', function() {
        expect(itemArray.addItem('test')).toBeFalsy();
    });

    it('returns true if item is removed', function() {
        expect(itemArray.removeItem('test')).toBeTruthy();
    });

    it('returns false if remove item is not in array', function () {
        expect(itemArray.removeItem('test')).toBeFalsy();
    });

    it('adds an array of items', function() {
        itemArray.addItems(['test1', 'test2', 'test3']);
        expect(itemArray.getItems()).toEqual(['test1', 'test2', 'test3']);
    });

    it('removes an array of items', function() {
        expect(
            itemArray.removeItems(['test1', 'test2', 'test3']))
            .toEqual(['test1', 'test2', 'test3']);
        expect(itemArray.contains('test1')).toBeFalsy();
    });

    it('returns true if item is in model and false otherwise', function() {
        itemArray.addItem('test4');
        expect(itemArray.contains('test4')).toBeTruthy();
        expect(itemArray.contains('test')).toBeFalsy();
    });

    it('returns a shallow copy of the item array', function() {
        let testArray = itemArray.getItems();
        testArray.push('testClassArray');
        expect(itemArray.contains('testClassArray')).toBeFalsy();
    });
});
