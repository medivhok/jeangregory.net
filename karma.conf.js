// Karma configuration
// Generated on Wed Apr 20 2016 08:37:55 GMT-0400 (EDT)
const webpackConfig = require('./webpack.config');

module.exports = function(config) {

    config.set({

        // base path that will be used to resolve all patterns (eg. files, exclude)
        basePath: '.',


        // frameworks to use
        // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
        frameworks: ['jasmine'],


        // list of files / patterns to load in the browser
        files: [
            { pattern: 'src/app/polyfills.ts', included: true, watched: false },
            { pattern: 'src/app/vendor.ts', included: true, watched: false },
            // { pattern: 'src/app/**/*.ts', included: true, watched: true },
            // { pattern: './karma-shim.js', watched: false }
            { pattern: 'src/app/test.index.ts', include: true, watched: true }
        ],


        // list of files to exclude
        exclude: [
            // 'src/app/**/index.ts',
            // 'src/app/**/*.d.ts'
        ],


        // preprocess matching files before serving them to the browser
        // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
        preprocessors: {
            'src/app/polyfills.ts': ['webpack'],
            'src/app/vendor.ts': ['webpack'],
            // './karma-shim.js': ['webpack', 'sourcemap']
            'src/app/test.index.ts' : ['webpack', 'sourcemap']
            // 'src/app/**/!(*spec).ts': ['webpack', 'coverage'],
            // 'src/app/**/*.spec.ts': ['webpack'],
        },

        coverageReporter: {
            reporters: [{
                type: 'json',
                dir: './report',
                subdir: 'coverage',
                file: 'coverage-final.json'
            },
        {
            type: 'html',
            dir: './report'
        }]
        },

        webpack: webpackConfig,

        webpackMiddleware: {
            stats: 'errors-only'
        },

        webpackServer: {
            noInfo: true
        },

        // test results reporter to use
        // possible values: 'dots', 'progress'
        // available reporters: https://npmjs.org/browse/keyword/karma-reporter
        reporters: ['progress', 'dots', 'kjhtml', 'coverage'],

        // htmlReporter: {
        //     outputDir: 'report/karma_html',
        //     template: null,
        //     focusOnFailures: true,
        //     namedFiles: false,
        //     pageTitle: null,
        //     urlFriendlyName: false,
        //     reportName: 'report-summary-filename',

            // experimental
        //     preserveDescribeNesting: false,
        //     foldAll: false
        // },


        // web server port
        port: 9876,


        // enable / disable colors in the output (reporters and logs)
        colors: true,


        // level of logging
        // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
        logLevel: config.LOG_INFO,


        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: true,


        // start these browsers
        // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
        browsers: ['Chrome'],


        // Continuous Integration mode
        // if true, Karma captures browsers, runs the tests and exits
        singleRun: false,

        // Concurrency level
        // how many browser should be started simultaneous
        concurrency: Infinity,
    })
}
